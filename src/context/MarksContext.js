import React, { useState, useEffect, createContext } from "react";
import connect from "./../api/connect";

export const MarksContext = createContext();

const MarksProvider = props => {
  const [marks, setMarks] = useState([]);
  useEffect(() => {
    connect
      .get("marks/")
      .then(res => {
        setMarks(res.data);
      })
      .catch(error => console.log(error));
  }, []);
  return (
    <div>
      <MarksContext.Provider value={[marks, setMarks]}>
        {props.children}
      </MarksContext.Provider>
    </div>
  );
};

export default MarksProvider;
