import React, { useState, useEffect, createContext } from "react";
import connect from "./../api/connect";

export const StudentsContext = createContext();

const StudentProvider = props => {
  const [students, setStudents] = useState([]);
  useEffect(() => {
    connect
      .get("student/")
      .then(res => {
        setStudents(res.data);
      })
      .catch(error => console.log(error));
  }, []);
  return (
    <div>
      <StudentsContext.Provider value={[students, setStudents]}>
        {props.children}
      </StudentsContext.Provider>
    </div>
  );
};

export default StudentProvider;
