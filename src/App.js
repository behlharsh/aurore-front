import React from "react";

import "./App.css";
import MarkSheet from "./components/MarkSheet";
import AddMarks from "./components/AddMarks";
import AddStudent from "./components/AddStudent";
import MarksProvider from "./context/MarksContext.js";
import StudentProvider from "./context/StudentContext.js";

function App() {
  return (
    <MarksProvider>
      <StudentProvider>
        <div className="App container">
          <div className="row">
            <div className="col m6">
              <h5>Add Student</h5>
              <AddStudent />
            </div>
            <div className="col m6">
              <h5>Add Mark</h5>
              <AddMarks />
            </div>
          </div>
          <hr />

          <MarkSheet />
        </div>
      </StudentProvider>
    </MarksProvider>
  );
}

export default App;
