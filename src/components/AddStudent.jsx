import React, { useState, useContext } from "react";
import connect from "./../api/connect";
import { StudentsContext } from "./../context/StudentContext";

const AddStudent = () => {
  const [state, setState] = useState({
    name: "",
    roll_no: "",
    standard: ""
  });

  const [student, setStudent] = useContext(StudentsContext);

  const handleChange = e => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    });
  };

  const addStudent = e => {
    e.preventDefault();
    connect
      .post("student/", state)
      .then(res => {
        console.log(res.data);
        setStudent([...student, res.data]);
        setState({ name: "", roll_no: "", standard: "" });
      })
      .catch(err => console.error(err));
  };

  return (
    <div>
      name: {state.name} roll no: {state.roll_no} statndard: {state.standard}
      <form onSubmit={addStudent}>
        <input
          type="text"
          name="name"
          placeholder="Enter Stundent Name"
          value={state.name}
          onChange={handleChange}
        />
        <input
          type="text"
          name="roll_no"
          placeholder="Enter Stundent Roll no"
          value={state.roll_no}
          onChange={handleChange}
        />
        <select
          name="standard"
          className="browser-default"
          value={state.standard}
          onChange={handleChange}
        >
          <option value="" disabled>
            Select Standard
          </option>
          <option value="10th">10th</option>
          <option value="11th">11th</option>
          <option value="12th">12th</option>
        </select>
        <br />
        <input type="submit" value="add" className="btn" />
      </form>
    </div>
  );
};

export default AddStudent;
