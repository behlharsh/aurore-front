import React, { useState, useEffect, useContext } from "react";
import connect from "./../api/connect";
import { MarksContext } from "./../context/MarksContext";

const AddMarks = () => {
  const [students, setStudents] = useState([]);
  const [student, setStudent] = useState("");
  const [math, setMath] = useState("");
  const [english, setEnglish] = useState("");
  const [science, setScience] = useState("");
  const [marks, setMarks] = useContext(MarksContext);

  useEffect(() => {
    connect
      .get("student/")
      .then(res => {
        setStudents(res.data);
      })
      .catch(err => console.log(err));
  }, []);
  const addMark = e => {
    e.preventDefault();
    connect
      .post("marks/", {
        english_marks: science,
        science_marks: english,
        maths_marks: math,
        student: student
      })
      .then(res => {
        setMarks([...marks, res.data]);
        setStudent("");
        console.log("aya");
        setMath("");
        console.log("fir aya");
        setEnglish("");
        setScience("");
        // console.log(res.data);
      })
      .catch(err => console.error(err));
  };

  return (
    <div>
      student: {student} english: {english} maths: {math} science: {science}
      <form onSubmit={addMark}>
        <select
          onChange={e => {
            setStudent(e.target.value);
          }}
          className="browser-default"
          value={student}
        >
          <option value="" disabled>
            Select Student
          </option>
          {students.map(student => {
            return (
              <option value={student.id} key={student.id}>
                {student.name}
              </option>
            );
          })}
        </select>
        <input
          type="text"
          onChange={e => {
            setEnglish(e.target.value);
          }}
          placeholder="Enter English Mark"
          value={english}
        />
        <input
          type="text"
          onChange={e => {
            setMath(e.target.value);
          }}
          placeholder="Enter Math Mark"
          value={math}
        />
        <input
          type="text"
          onChange={e => {
            setScience(e.target.value);
          }}
          placeholder="Enter Science Mark"
          value={science}
        />
        <br />
        <input type="submit" value="add" className="btn" />
      </form>
    </div>
  );
};

export default AddMarks;
