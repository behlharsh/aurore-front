import React, { useContext, useState } from "react";
// import connect from "./../api/connect";
import { MarksContext } from "./../context/MarksContext";
import { StudentsContext } from "./../context/StudentContext";
import _ from "lodash";

const MarkSheet = () => {
  const [marks] = useContext(MarksContext);
  const [search, setSearch] = useState("");
  const [search_stud, setSearchStudent] = useState("");
  const [students] = useContext(StudentsContext);
  const [sortOrder, setSortOrder] = useState(true);
  const [sortOrderStandard, setsortOrderStandard] = useState(true);

  const totalMarks = (m1, m2, m3) => {
    return parseFloat(m1) + parseFloat(m2) + parseFloat(m3);
  };

  const renderMarks = () => {
    let filterMarks = marks;
    if (search !== "") {
      filterMarks = filterMarks.filter(mark =>
        mark.student_name.toLowerCase().includes(search.toLowerCase())
      );
    }
    let order = sortOrder ? "asc" : "desc";
    filterMarks = _.orderBy(filterMarks, "student_name", order);

    return filterMarks.map(mark => {
      return (
        <tr key={mark.id}>
          <td>{mark.student_name}</td>
          <td>{mark.english_marks}</td>
          <td>{mark.science_marks}</td>
          <td>{mark.maths_marks}</td>
          <td>
            {totalMarks(
              mark.science_marks,
              mark.english_marks,
              mark.maths_marks
            )}
          </td>
        </tr>
      );
    });
  };

  // student

  const renderStudents = () => {
    let filterStudents = students;
    if (search_stud !== "") {
      filterStudents = filterStudents.filter(student =>
        student.name.toLowerCase().includes(search_stud.toLowerCase())
      );
    }
    let order = sortOrderStandard ? "asc" : "desc";
    filterStudents = _.orderBy(filterStudents, "roll_no", order);

    return filterStudents.map(student => {
      return (
        <tr key={student.id}>
          <td>{student.name}</td>
          <td>{student.roll_no}</td>
          <td>{student.standard}</td>
        </tr>
      );
    });
  };

  return (
    <div className="row">
      <div className="col m6">
        <h5>Student's Marks List</h5>
        <input
          type="text"
          value={search}
          placeholder="Search Name"
          onChange={e => {
            setSearch(e.target.value);
          }}
        />
        <input
          type="button"
          value="Sort By Name"
          name="cancel"
          onClick={() => {
            setSortOrder(!sortOrder);
          }}
          className="btn"
        />
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>English Marks</th>
              <th>Science Marks</th>
              <th>Math Marks</th>
              <th>Total</th>
            </tr>
          </thead>

          <tbody>{renderMarks()}</tbody>
        </table>
      </div>
      <div className="col m6">
        <h5>Student List</h5>
        <input
          type="text"
          value={search_stud}
          placeholder="Search Student Name"
          onChange={e => {
            setSearchStudent(e.target.value);
          }}
        />

        <input
          type="button"
          value="Sort by Roll"
          onClick={() => {
            setsortOrderStandard(!sortOrderStandard);
          }}
          className="btn"
        />
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Roll #</th>
              <th>Standard</th>
            </tr>
          </thead>

          <tbody>{renderStudents()}</tbody>
        </table>
      </div>
    </div>
  );
};

export default MarkSheet;
